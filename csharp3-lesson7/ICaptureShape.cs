﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace csharp3_lesson7
{
    public interface ICaptureShape: IShape
    {
        //HH: proerties for all shape classes
        int Points { get; set; }
        void OnCollected(Random random, Size boardSize);
    }
}
