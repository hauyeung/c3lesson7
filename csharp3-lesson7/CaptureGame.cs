﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace csharp3_lesson7
{
    public class CaptureGame
    {
        private Size _boardSize;
        public Size BoardSize { set { _boardSize = value; } }
        private Random _random = new Random();
        private Collector _collector;
        private ICaptureShape[] _captureShapes;

        public int CollectorHits {  get { return _collector.Collected; } }
        public int CollectorPoints {  get { return _collector.CollectedPoints; } }
        public Point CollectorPosition
        {
            set
            {
                value.X -= _collector.Dimensions.Width;
                value.Y -= _collector.Dimensions.Height;
                _collector.Location = value;
            }
        }

        public CaptureGame(int collectorShapeCount, Size boardSize)
        {
            
            _boardSize = boardSize;
            _captureShapes = new ICaptureShape[collectorShapeCount];
            _collector = new Collector(Color.Red, new Point(0, 0), new Size(30, 30));

            for (int i = 0; i < Convert.ToInt32(collectorShapeCount / 2); i++)
                _captureShapes[i] = new EllipseCaptureShape(_random, new Size(20, 20), _boardSize, Color.Green, 5);

            for (int i = Convert.ToInt32(collectorShapeCount / 2); i < Convert.ToInt32(collectorShapeCount * 0.75); i++)
                _captureShapes[i] = new EllipseCaptureShape(_random, new Size(20, 20), _boardSize, Color.Red, -5);

            for (int i = Convert.ToInt32(collectorShapeCount * 0.75); i < collectorShapeCount; i++)
                _captureShapes[i] = new RectangleCaptureShape(_random, new Size(20, 20), _boardSize, Color.Yellow, 1);

        }

        public CaptureGame(int collectorShapeCount, Size boardSize, int h, int pts)
        {

            _boardSize = boardSize;
            _captureShapes = new ICaptureShape[collectorShapeCount];
            _collector = new Collector(Color.Red, new Point(0, 0), new Size(30, 30), h, pts);
            for (int i = 0; i < Convert.ToInt32(collectorShapeCount / 2); i++)
                _captureShapes[i] = new EllipseCaptureShape(_random, new Size(20, 20), _boardSize, Color.Green, 5);

            for (int i = Convert.ToInt32(collectorShapeCount / 2); i < Convert.ToInt32(collectorShapeCount*0.75); i++)
                _captureShapes[i] = new EllipseCaptureShape(_random, new Size(20, 20), _boardSize, Color.Red, -5);

            for (int i = Convert.ToInt32(collectorShapeCount *0.75); i < collectorShapeCount; i++)
                _captureShapes[i] = new RectangleCaptureShape(_random, new Size(20, 20), _boardSize, Color.Yellow, 1);

        }

        public void DrawCollectorShapes(Graphics graphics)
        {

            // foreach in C# is read-only (immutable)
            foreach (ICaptureShape collectorShape in _captureShapes)
                collectorShape.Draw(graphics);
        }

        public void AnimateCollectorShapes()
        {
            for (int i = 0; i < _captureShapes.Length; i++)
            {               
                    _captureShapes[i].Animate(_boardSize);
                    if (_captureShapes[i].Hit(_collector.Location, _collector.Dimensions))
                        _collector.Collect(_captureShapes[i], _random, _boardSize);                
            }
        }

        public void AnimateCollectorShapes(int speed)
        {
            for (int i = 0; i < _captureShapes.Length; i++)
            {              
                    _captureShapes[i].Animate(_boardSize, speed);
                    if (_captureShapes[i].Hit(_collector.Location, _collector.Dimensions))
                        _collector.Collect(_captureShapes[i], _random, _boardSize);
            }
        }

        public void DrawCollector(Graphics graphics)
        {
            _collector.Draw(graphics);
        }

        public void Reset()
        {
            _collector.Reset();
        }

    }
}
