﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace csharp3_lesson7
{
    public interface IShape
    {
        //HH: properties for all shape classes
        Size Dimensions { get; set; }
        Point Location { get; set; }
        Color FillColor { get; set; }
        void Draw(Graphics graphics);
        bool Hit(Point location, Size dimensions);
        void Animate(Size boardSize);
        void Animate(Size boardSize, int speed);

    }
}
