﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp3_lesson7
{
    public partial class MainForm : Form
    {
        CaptureGame _captureGame;               // Collector game object
        private bool _playGame = true;          // Play/pause toggle
        private int _maxShapes = 10;            // Default maximum number of shapes

        public MainForm()
        {
            InitializeComponent();
        }

        private void mainPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (_playGame) _captureGame.CollectorPosition = new Point(e.X, e.Y);

        }

        private void mainPictureBox_Paint(object sender, PaintEventArgs e)
        {
            //HH: render dots and square pointer on the screen
            _captureGame.DrawCollector(e.Graphics);
            _captureGame.DrawCollectorShapes(e.Graphics);
            string gameStatus = "Hits: " + _captureGame.CollectorHits + "  -  Points: " + _captureGame.CollectorPoints;

            //HH: set font on screen
            using (Font font = new Font("Arial", 12, FontStyle.Bold))
            using (SolidBrush brush = new SolidBrush(Color.Black))
            {
                e.Graphics.DrawString(gameStatus, font, brush, new Point(5, 5));
            }
            gameTimer.Enabled = true;


        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            _captureGame = new CaptureGame(_maxShapes, mainPictureBox.ClientSize);

        }

        private void gameTimer_Tick(object sender, EventArgs e)
        {
            if (_playGame)
            {
                gameTimer.Enabled = false;
                _captureGame.AnimateCollectorShapes();
                mainPictureBox.Invalidate();
            }
            if (_captureGame.CollectorPoints == 10)
            {
                int h = _captureGame.CollectorHits;
                int pts = _captureGame.CollectorHits;
                _captureGame = new CaptureGame(20, mainPictureBox.ClientSize, h, 15);
            }
            if (_captureGame.CollectorPoints > 10 && _captureGame.CollectorPoints <50)
            {
                
                _captureGame.AnimateCollectorShapes(3);
            }
            if (_captureGame.CollectorPoints ==50)
            {
                int pts = _captureGame.CollectorHits;
                int h = _captureGame.CollectorHits;
                _captureGame = new CaptureGame(30, mainPictureBox.ClientSize, h,55);

            }
            if (_captureGame.CollectorPoints > 50)
            {
                _captureGame.AnimateCollectorShapes(6);
            }

        }

        private void pauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _playGame = !_playGame;
            pauseToolStripMenuItem.Checked = !_playGame;

        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _captureGame = null;
            _maxShapes = 10;
            _captureGame = new CaptureGame(_maxShapes, mainPictureBox.ClientSize);
            _captureGame.Reset();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            _captureGame.BoardSize = mainPictureBox.ClientSize;

        }

    }
}
