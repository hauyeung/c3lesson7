﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace csharp3_lesson7
{
    public class RectangleCaptureShape : Shape, ICaptureShape
    {
        private int _points;
        public RectangleCaptureShape(Random random, Size dimensions, Size boardSize, Color color, int points)
            : base(color)
        {
            _points = points;
            base.Dimensions = dimensions;
            base.Reset(random, boardSize);
        }


        public int Points
        {
            get { return _points; }
            set { _points = value; }
        }

        public override void Draw(Graphics graphics)
        {
            using (SolidBrush brush = new SolidBrush(base.FillColor))

                graphics.FillRectangle(brush, new Rectangle(base.Location, base.Dimensions));

        }

        public void OnCollected(Random random, Size boardSize)
        {
            base.Reset(random, boardSize);
        }

    }
}
